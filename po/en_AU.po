# English (Australia) translation for lomiri-addressbook-app
# Copyright (c) 2014 Rosetta Contributors and Canonical Ltd 2014
# This file is distributed under the same license as the lomiri-addressbook-app package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: lomiri-addressbook-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-12-03 05:45+0000\n"
"PO-Revision-Date: 2024-12-16 11:11+0000\n"
"Last-Translator: Distant Prince <distantprinceca@gmail.com>\n"
"Language-Team: English (Australia) <https://hosted.weblate.org/projects/"
"lomiri/address-book-app/en_AU/>\n"
"Language: en_AU\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.9-rc\n"
"X-Launchpad-Export-Date: 2017-02-17 05:51+0000\n"

#: data/lomiri-addressbook-app.desktop.in:6
#: data/lomiri-addressbook-app.desktop.in:7
#: src/imports/ABContactListPage.qml:248
msgid "Contacts"
msgstr "Contacts"

#: data/lomiri-addressbook-app.desktop.in:8
msgid "Contacts Address Book"
msgstr "Contacts Address Book"

#: data/lomiri-addressbook-app.desktop.in:9
msgid "Contacts;People;Numbers"
msgstr "Contacts;People;Numbers"

#: src/imports/BusyImportingDialog.qml:31
msgid "Importing..."
msgstr "Importing..."

#: src/imports/BusyImportingDialog.qml:40
#: src/imports/ABContactEditorPage.qml:38
#: src/imports/Lomiri/AddressBook/Base/RemoveContactsDialog.qml:79
#: src/imports/Lomiri/AddressBook/ContactView/ContactFetchError.qml:29
#: src/imports/ABContactListPage.qml:589
msgid "Cancel"
msgstr "Cancel"

#: src/imports/Settings/SettingsPage.qml:35
#: src/imports/ABContactListPage.qml:559
msgid "Settings"
msgstr "Settings"

#: src/imports/Settings/SettingsPage.qml:72
#, qt-format
msgid "My phone number: %1"
msgstr "My phone number: %1"

#: src/imports/Settings/SettingsPage.qml:72
msgid "Could not read phone number from SIM card"
msgstr "Could not read phone number from SIM card"

#: src/imports/Settings/SettingsPage.qml:73
#, qt-format
msgid "SIM %1"
msgstr "SIM %1"

#: src/imports/Settings/SettingsPage.qml:80
msgid "Reading my phone number(s)..."
msgstr "Reading my phone number(s)..."

#: src/imports/Settings/SettingsPage.qml:96
#, qt-format
msgid "Add %1 account"
msgstr "Add %1 account"

#: src/imports/Settings/SettingsPage.qml:119
msgid "Import from SIM"
msgstr "Import from SIM"

#: src/imports/Settings/SettingsPage.qml:136
msgid "Import from vCard file"
msgstr "Import from vCard file"

#: src/imports/Settings/SettingsPage.qml:158
msgid "Export all contacts"
msgstr "Export all contacts"

#: src/imports/Settings/SettingsDefaultSyncTarget.qml:173
msgid "Default address book"
msgstr "Default address book"

#: src/imports/Settings/SettingsDefaultSyncTarget.qml:201
#: src/imports/Lomiri/AddressBook/ContactEditor/ContactDetailSyncTargetEditor.qml:201
#: src/imports/Lomiri/AddressBook/ContactView/ContactDetailSyncTargetView.qml:88
#, qt-format
msgid "Personal - %1"
msgstr "Personal - %1"

#: src/imports/ABContactEditorPage.qml:56
msgid "Save"
msgstr "Save"

#: src/imports/Lomiri/AddressBook/Base/RemoveContactsDialog.qml:40
msgid "No contact selected."
msgstr "No contact selected."

#: src/imports/Lomiri/AddressBook/Base/RemoveContactsDialog.qml:44
msgid "Multiple contacts"
msgstr "Multiple contacts"

#: src/imports/Lomiri/AddressBook/Base/RemoveContactsDialog.qml:49
msgid "Are you sure that you want to remove this contact?"
msgstr "Are you sure that you want to remove this contact?"

#: src/imports/Lomiri/AddressBook/Base/RemoveContactsDialog.qml:51
msgid "Are you sure that you want to remove all selected contacts?"
msgstr "Are you sure that you want to remove all selected contacts?"

#: src/imports/Lomiri/AddressBook/Base/RemoveContactsDialog.qml:63
msgid "Remove"
msgstr "Remove"

#: src/imports/Lomiri/AddressBook/Base/BusyExportingDialog.qml:23
msgid "Exporting contacts..."
msgstr "Exporting contacts..."

#. TRANSLATORS: This refers to home landline phone label
#: src/imports/Lomiri/AddressBook/Base/ContactDetailGroupWithTypeBase.qml:116
#: src/imports/Lomiri/Contacts/ContactDetailPhoneNumberTypeModel.qml:105
msgid "Home"
msgstr "Home"

#. TRANSLATORS: This refers to landline work phone label
#: src/imports/Lomiri/AddressBook/Base/ContactDetailGroupWithTypeBase.qml:119
#: src/imports/Lomiri/Contacts/ContactDetailPhoneNumberTypeModel.qml:109
msgid "Work"
msgstr "Work"

#. TRANSLATORS: This refers to any other phone label
#: src/imports/Lomiri/AddressBook/Base/ContactDetailGroupWithTypeBase.qml:122
#: src/imports/Lomiri/Contacts/ContactDetailPhoneNumberTypeModel.qml:121
msgid "Other"
msgstr "Other"

#: src/imports/Lomiri/AddressBook/ContactEditor/ContactDetailAddressesEditor.qml:23
#: src/imports/Lomiri/AddressBook/ContactEditor/ComboButtonAddField.qml:55
#: src/imports/Lomiri/AddressBook/ContactView/ContactDetailAddressesView.qml:24
msgid "Address"
msgstr "Address"

#: src/imports/Lomiri/AddressBook/ContactEditor/ContactDetailAddressesEditor.qml:31
msgid "Street"
msgstr "Street"

#: src/imports/Lomiri/AddressBook/ContactEditor/ContactDetailAddressesEditor.qml:32
msgid "Locality"
msgstr "Locality"

#: src/imports/Lomiri/AddressBook/ContactEditor/ContactDetailAddressesEditor.qml:33
msgid "Region"
msgstr "Region"

#: src/imports/Lomiri/AddressBook/ContactEditor/ContactDetailAddressesEditor.qml:34
msgid "Post code"
msgstr "Post code"

#: src/imports/Lomiri/AddressBook/ContactEditor/ContactDetailAddressesEditor.qml:35
msgid "Country"
msgstr "Country"

#: src/imports/Lomiri/AddressBook/ContactEditor/ContactDetailEmailsEditor.qml:24
#: src/imports/Lomiri/AddressBook/ContactEditor/ComboButtonAddField.qml:53
#: src/imports/Lomiri/AddressBook/ContactView/ContactDetailEmailsView.qml:25
#: src/imports/Lomiri/AddressBook/ContactView/ContactDetailEmailsView.qml:28
msgid "Email"
msgstr "Email"

#: src/imports/Lomiri/AddressBook/ContactEditor/ContactDetailEmailsEditor.qml:28
msgid "Enter an email address"
msgstr "Enter an email address"

#: src/imports/Lomiri/AddressBook/ContactEditor/ContactEditorPage.qml:217
#: src/imports/Lomiri/Contacts/ContactListView.qml:499
msgid "New contact"
msgstr "New contact"

#: src/imports/Lomiri/AddressBook/ContactEditor/ContactEditorPage.qml:217
#: src/imports/ABContactViewPage.qml:87
msgid "Edit"
msgstr "Edit"

#: src/imports/Lomiri/AddressBook/ContactEditor/ContactEditorPage.qml:249
#: src/imports/Lomiri/AddressBook/ContactEditor/AlertMessageDialog.qml:29
msgid "Contact Editor"
msgstr "Contact Editor"

#: src/imports/Lomiri/AddressBook/ContactEditor/ContactEditorPage.qml:250
msgid "Could not save the contact. Please check the logs."
msgstr "Could not save the contact. Please check the logs."

#: src/imports/Lomiri/AddressBook/ContactEditor/ContactEditorPage.qml:253
#: src/imports/Lomiri/AddressBook/ContactEditor/AlertMessageDialog.qml:48
msgid "Close"
msgstr "Close"

#: src/imports/Lomiri/AddressBook/ContactEditor/ContactEditorPage.qml:495
msgid "Add field"
msgstr "Add field"

#: src/imports/Lomiri/AddressBook/ContactEditor/ContactEditorPage.qml:564
#: src/imports/ABContactListPage.qml:376 src/imports/ABContactListPage.qml:667
msgid "Delete"
msgstr "Delete"

#. TRANSLATORS: This is the text that will be used on the "return" key for the virtual keyboard,
#. this word must be less than 5 characters
#: src/imports/Lomiri/AddressBook/ContactEditor/TextInputDetail.qml:82
msgid "Next"
msgstr "Next"

#: src/imports/Lomiri/AddressBook/ContactEditor/ContactDetailUrlEditor.qml:76
#: src/imports/Lomiri/AddressBook/ContactEditor/ComboButtonAddField.qml:67
#: src/imports/Lomiri/AddressBook/ContactView/ContactDetailUrlView.qml:40
msgid "Web address"
msgstr "Web address"

#: src/imports/Lomiri/AddressBook/ContactEditor/ContactDetailUrlEditor.qml:91
msgid "Type URL…"
msgstr "Type URL…"

#: src/imports/Lomiri/AddressBook/ContactEditor/AlertMessageDialog.qml:33
#, qt-format
msgid ""
"Your <b>%1</b> contact sync account needs to be upgraded.\n"
"Wait until the upgrade is complete to edit contacts."
msgstr ""
"Your <b>%1</b> contact sync account needs to be upgraded.\n"
"Wait until the upgrade is complete to edit contacts."

#: src/imports/Lomiri/AddressBook/ContactEditor/AlertMessageDialog.qml:38
#, qt-format
msgid ""
"Your <b>%1</b> contact sync account needs to be upgraded. Use the sync "
"button to upgrade the Contacts app.\n"
"Only local contacts will be editable until upgrade is complete."
msgstr ""
"Your <b>%1</b> contact sync account needs to be upgraded. Use the sync "
"button to upgrade the Contacts app.\n"
"Only local contacts will be editable until upgrade is complete."

#: src/imports/Lomiri/AddressBook/ContactEditor/AlertMessageDialog.qml:42
#, qt-format
msgid ""
"Your <b>%1</b> contact sync account needs to be upgraded by running Contacts "
"app.\n"
"Only local contacts will be editable until upgrade is complete."
msgstr ""
"Your <b>%1</b> contact sync account needs to be upgraded by running Contacts "
"app.\n"
"Only local contacts will be editable until upgrade is complete."

#: src/imports/Lomiri/AddressBook/ContactEditor/ComboButtonAddField.qml:51
#: src/imports/Lomiri/AddressBook/ContactEditor/ContactDetailPhoneNumbersEditor.qml:23
#: src/imports/Lomiri/AddressBook/ContactView/ContactDetailPhoneNumbersView.qml:29
#: src/imports/Lomiri/AddressBook/ContactView/ContactDetailPhoneNumbersView.qml:32
msgid "Phone"
msgstr "Phone"

#: src/imports/Lomiri/AddressBook/ContactEditor/ComboButtonAddField.qml:57
#: src/imports/Lomiri/AddressBook/ContactView/ContactDetailOnlineAccountsView.qml:26
msgid "Social"
msgstr "Social"

#: src/imports/Lomiri/AddressBook/ContactEditor/ComboButtonAddField.qml:59
#: src/imports/Lomiri/AddressBook/ContactEditor/ContactDetailOrganizationsEditor.qml:21
#: src/imports/Lomiri/AddressBook/ContactView/ContactDetailOrganizationsView.qml:24
msgid "Professional Details"
msgstr "Professional Details"

#: src/imports/Lomiri/AddressBook/ContactEditor/ComboButtonAddField.qml:61
#: src/imports/Lomiri/AddressBook/ContactEditor/ContactDetailBirthdayEditor.qml:110
#: src/imports/Lomiri/AddressBook/ContactView/ContactDetailBirthdayView.qml:46
msgid "Birthday"
msgstr "Birthday"

#: src/imports/Lomiri/AddressBook/ContactEditor/ComboButtonAddField.qml:63
#: src/imports/Lomiri/AddressBook/ContactEditor/ContactDetailNoteEditor.qml:77
#: src/imports/Lomiri/AddressBook/ContactView/ContactDetailNoteView.qml:102
msgid "Note"
msgstr "Note"

#: src/imports/Lomiri/AddressBook/ContactEditor/ComboButtonAddField.qml:65
msgid "Ringtone"
msgstr "Ringtone"

#: src/imports/Lomiri/AddressBook/ContactEditor/ComboButtonAddField.qml:70
msgid "Middle Name"
msgstr "Middle Name"

#: src/imports/Lomiri/AddressBook/ContactEditor/ContactDetailOrganizationsEditor.qml:28
msgid "Organization"
msgstr "Organisation"

#: src/imports/Lomiri/AddressBook/ContactEditor/ContactDetailOrganizationsEditor.qml:29
msgid "Role"
msgstr "Role"

#: src/imports/Lomiri/AddressBook/ContactEditor/ContactDetailOrganizationsEditor.qml:30
msgid "Title"
msgstr "Title"

#: src/imports/Lomiri/AddressBook/ContactEditor/ContactDetailOnlineAccountsEditor.qml:23
msgid "IM"
msgstr "IM"

#: src/imports/Lomiri/AddressBook/ContactEditor/ContactDetailOnlineAccountsEditor.qml:27
msgid "Enter a social alias"
msgstr "Enter a social alias"

#: src/imports/Lomiri/AddressBook/ContactEditor/ContactDetailBirthdayEditor.qml:125
msgid "Enter a birthday"
msgstr "Enter a birthday"

#: src/imports/Lomiri/AddressBook/ContactEditor/ContactDetailNoteEditor.qml:94
msgid "Write note"
msgstr "Write note"

#: src/imports/Lomiri/AddressBook/ContactEditor/ContactDetailPhoneNumbersEditor.qml:27
msgid "Enter a number"
msgstr "Enter a number"

#: src/imports/Lomiri/AddressBook/ContactEditor/ContactDetailSyncTargetEditor.qml:179
#: src/imports/Lomiri/AddressBook/ContactView/ContactDetailSyncTargetView.qml:49
msgid "Addressbook"
msgstr "Addressbook"

#: src/imports/Lomiri/AddressBook/ContactEditor/ContactDetailNameEditor.qml:88
msgid "First name"
msgstr "First name"

#: src/imports/Lomiri/AddressBook/ContactEditor/ContactDetailNameEditor.qml:90
msgid "Middle name"
msgstr "Middle name"

#: src/imports/Lomiri/AddressBook/ContactEditor/ContactDetailNameEditor.qml:92
msgid "Last name"
msgstr "Last name"

#: src/imports/Lomiri/AddressBook/ContactView/ContactFetchError.qml:25
msgid "Error"
msgstr "Error"

#: src/imports/Lomiri/AddressBook/ContactView/ContactFetchError.qml:26
msgid "Contact not found"
msgstr "Contact not found"

#: src/imports/Lomiri/AddressBook/ContactView/ContactViewPage.qml:49
#: src/imports/Lomiri/AddressBook/ContactView/ContactViewPage.qml:58
#: src/imports/Lomiri/Contacts/ContactDelegate.qml:81
msgid "No name"
msgstr "No name"

#: src/imports/Lomiri/AddressBook/ContactView/ContactDetailOnlineAccountsView.qml:29
msgid "Touch"
msgstr "Touch"

#. TRANSLATORS: This refers to the AIM chat network http://en.wikipedia.org/wiki/AOL_Instant_Messenger
#: src/imports/Lomiri/Contacts/ContactDetailOnlineAccountTypeModel.qml:62
msgid "Aim"
msgstr "Aim"

#: src/imports/Lomiri/Contacts/ContactDetailOnlineAccountTypeModel.qml:63
msgid "ICQ"
msgstr "ICQ"

#: src/imports/Lomiri/Contacts/ContactDetailOnlineAccountTypeModel.qml:65
msgid "Jabber"
msgstr "Jabber"

#: src/imports/Lomiri/Contacts/ContactDetailOnlineAccountTypeModel.qml:66
msgid "MSN"
msgstr "MSN"

#: src/imports/Lomiri/Contacts/ContactDetailOnlineAccountTypeModel.qml:68
msgid "Skype"
msgstr "Skype"

#: src/imports/Lomiri/Contacts/ContactDetailOnlineAccountTypeModel.qml:69
msgid "Yahoo"
msgstr "Yahoo"

#. TRANSLATORS: this refers to a new contact
#: src/imports/Lomiri/Contacts/ContactListView.qml:491
msgid "+ Create New"
msgstr "+ Create New"

#: src/imports/Lomiri/Contacts/ContactListView.qml:555
#, qt-format
msgid "Import contacts from %1"
msgstr "Import contacts from %1"

#: src/imports/Lomiri/Contacts/ContactListView.qml:567
msgid "Import contacts from SIM card"
msgstr "Import contacts from SIM card"

#: src/imports/Lomiri/Contacts/ContactListView.qml:592
msgid "Import contacts from vcard file"
msgstr "Import contacts from vcard file"

#: src/imports/Lomiri/Contacts/ContactListView.qml:647
msgid "Synchronizing..."
msgstr "Synchronising..."

#: src/imports/Lomiri/Contacts/ContactListView.qml:647
#: src/imports/Lomiri/Contacts/SIMCardImportPage.qml:252
msgid "Loading..."
msgstr "Loading..."

#: src/imports/Lomiri/Contacts/ContactDetailPickerPhoneNumberDelegate.qml:82
msgid "Add number..."
msgstr "Add number..."

#. TRANSLATORS: This refers to mobile/cellphone phone label
#: src/imports/Lomiri/Contacts/ContactDetailPhoneNumberTypeModel.qml:113
msgid "Mobile"
msgstr "Mobile"

#. TRANSLATORS: This refers to mobile/cellphone work phone label
#: src/imports/Lomiri/Contacts/ContactDetailPhoneNumberTypeModel.qml:117
msgid "Work Mobile"
msgstr "Work Mobile"

#: src/imports/Lomiri/Contacts/SIMCardImportPage.qml:39
msgid "SIM contacts"
msgstr "SIM contacts"

#: src/imports/Lomiri/Contacts/SIMCardImportPage.qml:45
#: src/imports/ABContactListPage.qml:637
msgid "Unselect All"
msgstr "Unselect All"

#: src/imports/Lomiri/Contacts/SIMCardImportPage.qml:46
#: src/imports/ABContactListPage.qml:637
msgid "Select All"
msgstr "Select All"

#: src/imports/Lomiri/Contacts/SIMCardImportPage.qml:58
msgid "Import"
msgstr "Import"

#: src/imports/Lomiri/Contacts/SIMCardImportPage.qml:122
#, qt-format
msgid "%1 is locked"
msgstr "%1 is locked"

#: src/imports/Lomiri/Contacts/SIMCardImportPage.qml:124
msgid "Unlock"
msgstr "Unlock"

#: src/imports/Lomiri/Contacts/SIMCardImportPage.qml:155
msgid "No contacts found"
msgstr "No contacts found"

#: src/imports/Lomiri/Contacts/SIMCardImportPage.qml:261
msgid "Unlocking..."
msgstr "Unlocking..."

#: src/imports/Lomiri/Contacts/SIMCardImportPage.qml:269
msgid "Reading contacts from SIM..."
msgstr "Reading contacts from SIM..."

#: src/imports/Lomiri/Contacts/SIMCardImportPage.qml:277
msgid "Saving contacts on phone..."
msgstr "Saving contacts on phone..."

#: src/imports/Lomiri/Contacts/SIMCardImportPage.qml:285
msgid "Fail to read SIM card"
msgstr "Fail to read SIM card"

#: src/imports/ABContactViewPage.qml:74 src/imports/ABContactListPage.qml:386
#: src/imports/ABContactListPage.qml:650
msgid "Share"
msgstr "Share"

#: src/imports/ABMultiColumnEmptyState.qml:24
msgid "No contacts"
msgstr "No contacts"

#: src/imports/ABEmptyState.qml:56 src/imports/ABContactListPage.qml:791
msgid "Create a new contact by swiping up from the bottom of the screen."
msgstr "Create a new contact by swiping up from the bottom of the screen."

#: src/imports/ABContactListPage.qml:396
msgid "Message"
msgstr "Message"

#: src/imports/ABContactListPage.qml:403
msgid "Call"
msgstr "Call"

#: src/imports/ABContactListPage.qml:437
msgid "Please select a phone number"
msgstr "Please select a phone number"

#: src/imports/ABContactListPage.qml:450
msgid "Numbers"
msgstr "Numbers"

#: src/imports/ABContactListPage.qml:485
msgid "Search…"
msgstr "Search…"

#: src/imports/ABContactListPage.qml:501
msgid "Quit"
msgstr "Quit"

#: src/imports/ABContactListPage.qml:511
msgid "Search"
msgstr "Search"

#: src/imports/ABContactListPage.qml:547
msgid "Syncing"
msgstr "Syncing"

#: src/imports/ABContactListPage.qml:547
msgid "Sync"
msgstr "Sync"

#. TRANSLATORS: this refers to all contacts
#: src/imports/ABContactListPage.qml:569
msgid "All"
msgstr "All"

#: src/imports/ABContactListPage.qml:569
msgid "Favorites"
msgstr "Favourites"

#: src/imports/ABContactListPage.qml:627
msgid "Cancel selection"
msgstr "Cancel selection"

#: src/imports/ABContactListPage.qml:709
msgid "Back"
msgstr "Back"

#: src/imports/ABContactListPage.qml:722
msgid "Imported contacts"
msgstr "Imported contacts"

#: src/imports/ABContactListPage.qml:790
msgid "You have no contacts."
msgstr "You have no contacts."

#: src/imports/ABContactListPage.qml:888
msgid "Fail to import contacts!"
msgstr "Failed to import contacts!"

#~ msgid "Search..."
#~ msgstr "Search..."

#~ msgid "Add Field"
#~ msgstr "Add Field"

#~ msgid "No"
#~ msgstr "No"

#~ msgid "Yes"
#~ msgstr "Yes"

#~ msgid "Frequently called"
#~ msgstr "Frequently called"

#~ msgid "%1 vCards imported"
#~ msgstr "%1 vCards imported"

#~ msgid "Add contact"
#~ msgstr "Add contact"

#~ msgid "Import vCards"
#~ msgstr "Import vCards"

#~ msgid ""
#~ "Would you like to sync contacts\n"
#~ "from online accounts now?"
#~ msgstr ""
#~ "Would you like to sync contacts\n"
#~ "from online accounts now?"

#~ msgid ""
#~ "Would you like to sync contacts from your google online accounts now?"
#~ msgstr ""
#~ "Would you like to sync contacts from your google online accounts now?"
